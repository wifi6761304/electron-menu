// Using module syntax: rename forge.config.js into forge.config.cjs, add "type": "module" to package.json
import { app, BrowserWindow, Menu, dialog } from "electron";
import fs from "fs";
import path from "node:path";
import { fileURLToPath } from "node:url";
import { dirname } from "node:path";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (process.platform === "win32") {
	import("electron-squirrel-startup").then((module) => {
		if (module.default) {
			app.quit();
		}
	});
}

const createWindow = () => {
	// Create the browser window.
	const mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			preload: path.join(__dirname, "preload.js"),
		},
	});

	// and load the index.html of the app.
	mainWindow.loadFile(path.join(__dirname, "index.html"));

	const menuTemplate = [
		{ role: 'appMenu' },
		// ...(isMac
		// 	? [
		// 			{
		// 				label: app.name,
		// 				submenu: [
		// 					{ role: "about" },
		// 					{ type: "separator" },
		// 					{ role: "services" },
		// 					{ type: "separator" },
		// 					{ role: "hide" },
		// 					{ role: "hideOthers" },
		// 					{ role: "unhide" },
		// 					{ type: "separator" },
		// 					{ role: "quit" },
		// 				],
		// 			},
		// 	  ]
		// 	: []),
		{
			label: "File",
			submenu: [
				{
					label: "Open CSV",
					click() {
						dialog
							.showOpenDialog({
								properties: ["openFile"],
								filters: [{ name: "CSV", extensions: ["csv"] }],
							})
							.then((result) => {
								// Wenn der Dialog nicht abgebrochen wurde bzw. eine Datei ausgewählt wurde
								if (
									!result.canceled &&
									result.filePaths.length > 0
								) {
									// Nur das erste ausgewählte Element wird geöffnet
									const filePath = result.filePaths[0];
									fs.readFile(
										filePath,
										"utf-8",
										(err, data) => {
											if (err) {
												console.error(
													"Error reading the file"
												);
												return;
											}
											mainWindow.webContents.send(
												"csv-data",
												data
											);
										}
									);
								}
							});
					},
				},
				{
					label: "Test Click",
					click() {
						mainWindow.webContents.send("test-click", "Test Click");
					}
				}
			],
		},
		{
			label: "Edit",
			submenu: [
				{ role: "undo" },
				{ role: "redo" },
				{ type: "separator" },
				{ role: "cut" },
				{ role: "copy" },
				{ role: "paste" },
				/*   ...(isMac
					? [
						{ role: 'pasteAndMatchStyle' },
						{ role: 'delete' },
						{ role: 'selectAll' },
						{ type: 'separator' },
						{
						  label: 'Speech',
						  submenu: [
							{ role: 'startSpeaking' },
							{ role: 'stopSpeaking' }
						  ]
						}
					  ]
					: [
						{ role: 'delete' },
						{ type: 'separator' },
						{ role: 'selectAll' }
					  ])
			*/
			],
		},
	];

	const menu = Menu.buildFromTemplate(menuTemplate);
	Menu.setApplicationMenu(menu);
	// Open the DevTools.
	mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
	createWindow();

	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	app.on("activate", () => {
		if (BrowserWindow.getAllWindows().length === 0) {
			createWindow();
		}
	});
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
	if (process.platform !== "darwin") {
		app.quit();
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
