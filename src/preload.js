// See the Electron documentation for details on how to use preload scripts:
// https://www.electronjs.org/docs/latest/tutorial/process-model#preload-scripts
const { contextBridge, ipcRenderer } = require("electron");

// Kann (noch) nicht als module importiert werden. Elektron hat eine eigene Implementierung.
// import { contextBridge, ipcRenderer } from "electron";

contextBridge.exposeInMainWorld("api", {
    // generisch alle Events abonnieren
	receive: (channel, func) => {
		ipcRenderer.on(channel, (event, ...args) => func(...args));
	},
});
